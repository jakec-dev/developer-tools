# [1.3.0](https://gitlab.com/jakec-dev/developer-tools/compare/v1.2.0...v1.3.0) (2024-03-24)


### Features

* **multiple:** asdg ([3fbc581](https://gitlab.com/jakec-dev/developer-tools/commit/3fbc581440d631d3f49cb836ad59f087d62fce87))
* **multiple:** asdgasdg ([267c47a](https://gitlab.com/jakec-dev/developer-tools/commit/267c47a5d5eac498475c20708c1ebf1b9bf1ca9a))
* **multiple:** asdgasdhgasd ([6b12eb6](https://gitlab.com/jakec-dev/developer-tools/commit/6b12eb618179daf004f51ecaf97a300e37bdae6b))

# [1.2.0](https://gitlab.com/jakec-dev/developer-tools/compare/v1.1.0...v1.2.0) (2024-03-24)


### Bug Fixes

* **multiple:** :wq ([a42885b](https://gitlab.com/jakec-dev/developer-tools/commit/a42885b3fbccb9c913f3e1451091bc1b5a70bf18))


### Features

* **multiple:** asdhgas ([585621b](https://gitlab.com/jakec-dev/developer-tools/commit/585621be0d3959b6e1d3f4fe07bb31d3645822fd))
* **multiple:** easedgasedg ([c69f7e2](https://gitlab.com/jakec-dev/developer-tools/commit/c69f7e2d2c9bf032d4611f765e1d6eb93e239b2c))

# [1.1.0](https://gitlab.com/jakec-dev/developer-tools/compare/v1.0.1...v1.1.0) (2024-03-24)


### Features

* **multiple:** asdhgas ([325a271](https://gitlab.com/jakec-dev/developer-tools/commit/325a271358d37f5fc3655aa897350fb791770e89))

## [1.0.1](https://gitlab.com/jakec-dev/developer-tools/compare/v1.0.0...v1.0.1) (2024-03-24)

# 1.0.0 (2024-03-24)


### Bug Fixes

* **eslint:** ewyaw ([086406b](https://gitlab.com/jakec-dev/developer-tools/commit/086406be19392e37b96f34910c1ad588f9d0c24e))
* **eslint:** Temp ([0496d63](https://gitlab.com/jakec-dev/developer-tools/commit/0496d6365e006753d348d280cfa4f494d5cc9479))
* **eslint:** testing ([0f5dc53](https://gitlab.com/jakec-dev/developer-tools/commit/0f5dc5398c847acdacc570ed7584381a6f0422ee))
* Fix type in gitlab-ci ([1825b87](https://gitlab.com/jakec-dev/developer-tools/commit/1825b87f22b5bcb0ea11765ad9a1b7dbb6d7ce01))
* **multiple:** hjklh ([b303504](https://gitlab.com/jakec-dev/developer-tools/commit/b30350429f68470cda757e63a412b70b761dbbea))
* **multiple:** sdfghsadfh ([457a93f](https://gitlab.com/jakec-dev/developer-tools/commit/457a93f66f2629f4efe5352e882384888e6a6761))
* **multiple:** waehaweh ([5d81f72](https://gitlab.com/jakec-dev/developer-tools/commit/5d81f72b24090c67e9c361cca108d175a8521344))
* **prettier:** yye ([418d313](https://gitlab.com/jakec-dev/developer-tools/commit/418d31338bd698eba8505da4dd1cae03aca323c1))
* **semantic-versioning:** [[AWR-384](https://localsearch.atlassian.net/browse/AWR-384)] Change location ([5fa36e8](https://gitlab.com/jakec-dev/developer-tools/commit/5fa36e810258e7e2c0b8abafcea3f8314cda519b))
* **semantic-versioning:** ew ([a3c8413](https://gitlab.com/jakec-dev/developer-tools/commit/a3c841390940358d2adc840c88b377ec34588964))


* feat(husky)!: Testing ([49d619b](https://gitlab.com/jakec-dev/developer-tools/commit/49d619b093b279dfcedef17fcf9e7e065fda72d9))


### Features

* **eslint:** A new feature ([920d8c1](https://gitlab.com/jakec-dev/developer-tools/commit/920d8c1494e8f8d6b7476e396d8d65bd6d5a3cde))
* **eslint:** a3gaws ([5d92e78](https://gitlab.com/jakec-dev/developer-tools/commit/5d92e78f25c5f58a3034beaa6662376722bf9d41))
* **eslint:** aedgasd ([c016209](https://gitlab.com/jakec-dev/developer-tools/commit/c016209dea9eac367906b9c2372f7f6c4199cd09))
* **eslint:** asdf ([77923cc](https://gitlab.com/jakec-dev/developer-tools/commit/77923cc9d439518fdaa58b7f039350b3114d486b))
* **eslint:** asdf ([fd5e810](https://gitlab.com/jakec-dev/developer-tools/commit/fd5e810855fa90dc6f90290050520cf01595bef0))
* **eslint:** asdf ([c57a418](https://gitlab.com/jakec-dev/developer-tools/commit/c57a418a56e778479e476c0c1dc01f624413b9c4))
* **eslint:** asdf ([bd010b6](https://gitlab.com/jakec-dev/developer-tools/commit/bd010b676ef156a8ca665aa5b1fd7747e71ee47c))
* **eslint:** asdf ([e3a5ed5](https://gitlab.com/jakec-dev/developer-tools/commit/e3a5ed5e3f970b327b4af64e0c3445efe888a7a4))
* **eslint:** asdfasdef ([5ac6ab2](https://gitlab.com/jakec-dev/developer-tools/commit/5ac6ab26c6fb33e9c001a940fc6496d301138e79))
* **eslint:** asdfasdf ([24b418f](https://gitlab.com/jakec-dev/developer-tools/commit/24b418f24ee47d0d37937327a3e89accbf1140b9))
* **eslint:** asdfawef ([4dcd008](https://gitlab.com/jakec-dev/developer-tools/commit/4dcd0088bfc85ab677869ca5aa082b672ac3a167))
* **eslint:** asdfewag43 ([53b8c82](https://gitlab.com/jakec-dev/developer-tools/commit/53b8c823538552aae3b0a6a44cdd124d0dc7e814))
* **eslint:** asdgasdg ([5ba243f](https://gitlab.com/jakec-dev/developer-tools/commit/5ba243fa8f280efed62c4be91483e6dbb1a67aba))
* **eslint:** asdgasdg ([a7fab35](https://gitlab.com/jakec-dev/developer-tools/commit/a7fab35c5d44c814faf106401865a652df6ed466))
* **eslint:** asewgfaweg ([af01a9e](https://gitlab.com/jakec-dev/developer-tools/commit/af01a9e88dcc1ad5a2a0ba99d0483b58cdbb5747))
* **eslint:** awestawseg ([4a21761](https://gitlab.com/jakec-dev/developer-tools/commit/4a21761b4262d9d3dcd02493d7e233fc45d9b7e2))
* **eslint:** awresgsdh ([1b83642](https://gitlab.com/jakec-dev/developer-tools/commit/1b83642633e64847dabab7969ba8d06d057bd7eb))
* **eslint:** dagsdh ([f9e2ee1](https://gitlab.com/jakec-dev/developer-tools/commit/f9e2ee1bc017db19e555fbc44aae13aeaafe63dd))
* **eslint:** dsgasdgasdg ([2a48935](https://gitlab.com/jakec-dev/developer-tools/commit/2a48935a161316f42c3416a89ef8d6d8af9b2aba))
* **eslint:** eage ([ac40f48](https://gitlab.com/jakec-dev/developer-tools/commit/ac40f4808614762f10fab381e633c073b719d764))
* **eslint:** easweg ([8213b15](https://gitlab.com/jakec-dev/developer-tools/commit/8213b155c4a6a775f7a01140cf327299a06dcf22))
* **eslint:** eaw ([96630c9](https://gitlab.com/jakec-dev/developer-tools/commit/96630c9b4944ca7744f3ac7d16c0c031cd640ff7))
* **eslint:** eawgf34 ([fd9ac13](https://gitlab.com/jakec-dev/developer-tools/commit/fd9ac137abcd0f1ad5e7fd043dae449b07b4650b))
* **eslint:** eawtaewt ([679b53e](https://gitlab.com/jakec-dev/developer-tools/commit/679b53e52e0ea454645a3786952c96260edbad07))
* **eslint:** efawef ([e51f089](https://gitlab.com/jakec-dev/developer-tools/commit/e51f08911ce15a4cce0c6fd17b5077e5522216c8))
* **eslint:** ewafawef ([afb1e38](https://gitlab.com/jakec-dev/developer-tools/commit/afb1e38db5e6080651f37fb0299df51413f32d36))
* **eslint:** ewgawe ([f9e3a8c](https://gitlab.com/jakec-dev/developer-tools/commit/f9e3a8c74e93e2ff0456be2500422a6eba279f24))
* **eslint:** hnjk;hk ([200a0dc](https://gitlab.com/jakec-dev/developer-tools/commit/200a0dc0a80d6642a1225b54771fae9f7c517cda))
* **eslint:** ko ([bc78c93](https://gitlab.com/jakec-dev/developer-tools/commit/bc78c93c95cd9670049c245cd94d479d2eb51e64))
* **eslint:** qetawsdg ([748afb3](https://gitlab.com/jakec-dev/developer-tools/commit/748afb32cb88ff7810cf2b0a37b7cf3dbae1acd9))
* **eslint:** remove cache ([52563ea](https://gitlab.com/jakec-dev/developer-tools/commit/52563eac4e001e2ce807d41c0b65a19848766dee))
* **eslint:** sadfasdg ([39e357f](https://gitlab.com/jakec-dev/developer-tools/commit/39e357f97f2a3b57352e7844e7d7c893e52446a1))
* **eslint:** sadgaweg ([384bfca](https://gitlab.com/jakec-dev/developer-tools/commit/384bfcad60522f0a2d1abd21f179e5027107165e))
* **eslint:** sadhgaweha ([4f5d2b2](https://gitlab.com/jakec-dev/developer-tools/commit/4f5d2b23e05233f67c0d2a0292e243c9872f5553))
* **eslint:** saewagwe ([1de3f7a](https://gitlab.com/jakec-dev/developer-tools/commit/1de3f7adfc996e547bdaed0f861cfcfe995f69c0))
* **eslint:** TE ([76bf524](https://gitlab.com/jakec-dev/developer-tools/commit/76bf52403db2c5f96b9d9ad85990b4b264a30c58))
* **eslint:** tea ([32d4c88](https://gitlab.com/jakec-dev/developer-tools/commit/32d4c887121d54367df6dde483c693347870ada0))
* **eslint:** tes ([c58f4fd](https://gitlab.com/jakec-dev/developer-tools/commit/c58f4fd11fc5651d8057e8d56bb4d7fa35fbf6e6))
* **eslint:** tesagasdg ([19a2c21](https://gitlab.com/jakec-dev/developer-tools/commit/19a2c214e5f5f0a90e8619967687d037a29eb6ae))
* **eslint:** test ([073b859](https://gitlab.com/jakec-dev/developer-tools/commit/073b859d0bd32d2e3959ab775edb2c543281a701))
* **eslint:** Test ([a75e3c7](https://gitlab.com/jakec-dev/developer-tools/commit/a75e3c7857e2b4ef73f7caf35f6a19828623e94b))
* **eslint:** Testing ([213bdb0](https://gitlab.com/jakec-dev/developer-tools/commit/213bdb04dc1cdfbf4bd1e5aa62d4765d437292e8))
* **eslint:** Testing ([509f3cc](https://gitlab.com/jakec-dev/developer-tools/commit/509f3cc0dc52347bf1f6393f375ee0573a97a0a8))
* **eslint:** testing again ([a41c926](https://gitlab.com/jakec-dev/developer-tools/commit/a41c926f8ec4215e89a0fa1816f48b02af4f7c9d))
* **eslint:** Testing semantic versioning ([9498e73](https://gitlab.com/jakec-dev/developer-tools/commit/9498e73a2a0d5c34a1351f8ad1ed94112ab723ec))
* **eslint:** yes ([a3a5e69](https://gitlab.com/jakec-dev/developer-tools/commit/a3a5e6909ec9be76f575af0c656628074b5f04b7))
* **eslint:** yseadg ([7732390](https://gitlab.com/jakec-dev/developer-tools/commit/7732390d2469f84a7e564f424d7403c37d3edbff))
* **husky:** efawef ([8743b90](https://gitlab.com/jakec-dev/developer-tools/commit/8743b90e1de7bfc9272083da69748a24781cc951))
* Initial commit ([b1d24d5](https://gitlab.com/jakec-dev/developer-tools/commit/b1d24d5cc74739699fe25f7d332db9537ef98112))
* **multiple:** asdhgas ([95985e8](https://gitlab.com/jakec-dev/developer-tools/commit/95985e8cb2626a21477eb0a161861b0c87c62c7d))
* **multiple:** asdhgasdh ([a91cef3](https://gitlab.com/jakec-dev/developer-tools/commit/a91cef3d914472ae0f1e73de3183d1c925290883))
* **multiple:** awdsawshe ([ef06fcb](https://gitlab.com/jakec-dev/developer-tools/commit/ef06fcb72e867cabb65973841d2e27c471ca4480))
* **multiple:** awehaweh ([8e88679](https://gitlab.com/jakec-dev/developer-tools/commit/8e886797041ba03d71e24b73323b6be016dae65d))
* **multiple:** awsetawsegt ([05aece8](https://gitlab.com/jakec-dev/developer-tools/commit/05aece865a7b67d77fe88e0d74f4a6974b2765eb))
* **multiple:** Big one ([e187091](https://gitlab.com/jakec-dev/developer-tools/commit/e187091c15f599ce14528a3db5b14a93526ac9db))
* **multiple:** eah ([e68fc9e](https://gitlab.com/jakec-dev/developer-tools/commit/e68fc9ecf9560435765c4c784d228caf38a00c17))
* **multiple:** eawgaweh ([ed6de07](https://gitlab.com/jakec-dev/developer-tools/commit/ed6de07fc12bb8f3ae0a21b81e49da3cc26bef04))
* **multiple:** eawgawher ([be03f72](https://gitlab.com/jakec-dev/developer-tools/commit/be03f7261011deff4895974d4aa859abf5f025e8))
* **multiple:** eawseh ([68a8d89](https://gitlab.com/jakec-dev/developer-tools/commit/68a8d890f475b12ea455bf0de0950381c7365963))
* **multiple:** egaweg ([8fa5345](https://gitlab.com/jakec-dev/developer-tools/commit/8fa53456f2308b8bffe17f3ddb2819fbc1406fbb))
* **multiple:** egaweg ([a622709](https://gitlab.com/jakec-dev/developer-tools/commit/a622709e50cc54581edc431a9b50dddb9d2b3170))
* **multiple:** ewahwrh ([67f0659](https://gitlab.com/jakec-dev/developer-tools/commit/67f065969f8df2557d0667f4cb3edb5581374e80))
* **multiple:** hgk ([3fdebba](https://gitlab.com/jakec-dev/developer-tools/commit/3fdebbaf62a1ce60b5235a5665b6fe30aa0a9b1d))
* **multiple:** jake ([501ea13](https://gitlab.com/jakec-dev/developer-tools/commit/501ea131d4ea9a32ae1abe76d0f334c7b39f7b83))
* **multiple:** mknjk ([6b5e858](https://gitlab.com/jakec-dev/developer-tools/commit/6b5e85878c521bc04a9ad10edca5c94b11c7bdb4))
* **multiple:** rawehwsh ([c80ded6](https://gitlab.com/jakec-dev/developer-tools/commit/c80ded6b6ce151c74a8ceb3c27ca89626eff871b))
* **multiple:** sdgasdg ([1912a1c](https://gitlab.com/jakec-dev/developer-tools/commit/1912a1ccf786ce040e0080fa9595af411b6e33c0))
* **multiple:** Testing ([e534b07](https://gitlab.com/jakec-dev/developer-tools/commit/e534b07e4d5cdc870ee01fd4f9e1f568ec0d67a0))
* **multiple:** update gitlab ci ([243bc58](https://gitlab.com/jakec-dev/developer-tools/commit/243bc585da8252609bf9454bb36ddc65d0f3a7fa))
* **multiple:** weahjawehawhe ([390b501](https://gitlab.com/jakec-dev/developer-tools/commit/390b5010d0e2b77c706f35d370a89fe29a2bd965))
* **prettier:** yeaw ([9793a74](https://gitlab.com/jakec-dev/developer-tools/commit/9793a74493a515d4649d3eb7510520b0b7cc6a55))
* **prettier:** yes ([02c5b6d](https://gitlab.com/jakec-dev/developer-tools/commit/02c5b6d5599c1400f5f7fa45d80332a485f2ec4e))
* **typescript:** [[AWR-123](https://localsearch.atlassian.net/browse/AWR-123)] eafwegawehaweh ([47ed902](https://gitlab.com/jakec-dev/developer-tools/commit/47ed902a41b1de2761e42ce854381e06c9e9fafd))


### BREAKING CHANGES

* Move from monorepo
