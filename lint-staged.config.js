import createLintStagedConfig from './src/lint-staged/index.js';

export default createLintStagedConfig({ tsc: false });
