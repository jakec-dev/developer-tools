import { readFile } from 'fs/promises';
import commonjs from '@rollup/plugin-commonjs';
import terser from '@rollup/plugin-terser';
import copy from 'rollup-plugin-copy';

const packageJson = JSON.parse(await readFile(new URL('./package.json', import.meta.url)));

const BUILD_PACKAGES = [
  '',
  { name: '/commitizen', input: 'src/commitizen/index.cjs' },
  '/eslint',
  '/lint-staged',
  '/prettier',
  '/semantic-release',
];

export default BUILD_PACKAGES.map((pkg) => {
  const pkgName = typeof pkg === 'string' ? pkg : pkg.name;
  const input = typeof pkg === 'string' ? `src${pkg}/index.js` : pkg.input;
  return {
    input,
    output: [
      {
        file: packageJson.exports[`.${pkgName}`].import,
        format: 'es',
      },
      {
        file: packageJson.exports[`.${pkgName}`].require,
        format: 'cjs',
      },
    ],
    plugins: [
      commonjs(),
      copy({
        targets: [{ src: 'src/tsconfig', dest: 'dist' }],
      }),
      terser(),
    ],
    external: [
      ...Object.keys(packageJson.dependencies || []),
      'path',
      'url',
      '@digitalroute/cz-conventional-changelog-for-jira/configurable.js',
    ],
  };
});
