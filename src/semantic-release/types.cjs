const types = {
  feat: {
    description: 'A new feature',
    title: 'Features',
    release: 'minor',
  },
  fix: {
    description: 'A bug fix',
    title: 'Bug Fixes',
    release: 'patch',
  },
  revisions: {
    description: 'Changes to a currently open merge request (MR) based on code review feedback',
    title: 'Code Review Revisions',
    release: false,
  },
  qa: {
    description: 'Fixes for issues raised by Design, Functional or Release QA',
    title: 'QA Fixes',
    release: false,
  },
  style: {
    description: 'Cosmetic style updates that do not add, remove or change functionality',
    title: 'UI Changes',
    release: 'patch',
  },
  refactor: {
    description:
      'A code change that neither fixes a bug nor adds a feature (formatting, performance improvement, etc)',
    title: 'Code Refactoring',
    release: 'patch',
  },
  build: {
    description: 'Changes that affect the build system or external dependencies (pnpm, vite, etc.)',
    title: 'Builds',
    release: 'patch',
  },
  ci: {
    description: 'Changes to our CI configuration files and scripts',
    title: 'Continuous Integrations',
    release: 'patch',
  },
  docs: {
    description: 'Documentation only changes',
    title: 'Documentation',
    release: 'patch',
  },
  test: {
    description: 'Adding missing tests or correcting existing tests',
    title: 'Tests',
    release: 'patch',
  },
  save: {
    description: 'Temporary commit to save work in progress (note: must be rebased later).',
    title: 'Save Progress',
    release: false,
  },
  skip: {
    description: "Other changes that don't modify src or test files (devDependency updates, etc.)",
    title: 'Non-Release Changes',
    release: false,
  },
};

module.exports = types;
