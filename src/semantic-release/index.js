/* eslint-disable no-template-curly-in-string */
import types from './types.cjs';

/**
 * Generates a semantic-release configuration object.
 *
 * This configuration object should be imported in the project's `release.config.js` file. It can be expanded with additional semantic-release configuration options.
 *
 * @param options - Optional. Configuration options.
 * @param {boolean} options.gitlabRelease - Optional. Whether to generate a Gitlab release. Defaults to true.
 *
 * @see https://semantic-release.gitbook.io/semantic-release/usage/configuration#options
 *
 * @example
 * ./release.config.js
 * ```javascript
 * import { semanticVersioning } from '@jakec-dev/developer-tools';
 *
 * export default {
 *    ...semanticVersioning.createReleaseConfig({ gitlabRelease: false }),
 *    debug: true,
 * };
 * ```
 */
function createReleaseConfig({ gitlabRelease = true } = {}) {
  /**
   * @type {import('semantic-release').GlobalConfig}
   */
  const semanticReleaseConfig = {
    branches: [
      { name: 'master' },
      '+([0-9])?(.{+([0-9]),x}).x',
      { name: 'beta', prerelease: true },
    ],
    plugins: [
      /**
       * Required to analyse the commits.
       *
       * @see https://github.com/semantic-release/commit-analyzer
       */
      [
        '@semantic-release/commit-analyzer',
        {
          preset: 'conventionalcommits',
          releaseRules: Object.entries(types).reduce(
            (agg, [id, { release }]) =>
              release ?
                [
                  ...agg,
                  {
                    type: id,
                    release,
                  },
                ]
              : agg,
            [],
          ),
        },
      ],

      /**
       * Add links to JIRA issues in the release notes.
       *
       * This plugin uses @semantic-release/release-notes-generator under the hood, so it isn't needed anymore.
       *
       * @see https://github.com/iamludal/semantic-release-jira-notes
       */
      [
        'semantic-release-jira-notes',
        {
          jiraHost: 'localsearch.atlassian.net',
          ticketPrefixes: ['AWR'],
        },
      ],

      /**
       * Creates or updates the CHANGELOG.md file.
       *
       * @see https://github.com/semantic-release/changelog
       */
      '@semantic-release/changelog',

      /**
       * Publishes a GitLab release.
       *
       * @see https://github.com/semantic-release/gitlab
       */
      ...(gitlabRelease ? ['@semantic-release/gitlab'] : []),

      /**
       *
       * Only used to update package.json version. We disable the actual publish to npm step.
       *
       * @see https://github.com/semantic-release/npm
       */
      [
        '@semantic-release/npm',
        {
          npmPublish: false,
        },
      ],

      /**
       * Commits assets created or updated during the release process to the project's git repository.
       *
       * @see https://github.com/semantic-release/git
       */
      [
        '@semantic-release/git',
        {
          assets: ['package.json', 'CHANGELOG.md'],
          message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}',
        },
      ],
    ],
    tagFormat: 'v${version}',
    // Dev options
    dryRun: false,
    ci: true,
    debug: false,
  };

  return semanticReleaseConfig;
}

export default createReleaseConfig;
