/**
 * TODO: Write JSDocs.
 *
 * @param options - Configuration options.
 * @param options.lint - Whether to lint files. Defaults to true.
 * @param options.format - Whether to format files. Defaults to true.
 * @param options.tsc - Whether to type check files. Defaults to true.
 */
function createLintStagedConfig({ lint = true, format = true, tsc = true }) {
  const config = {};

  if (lint) {
    config['*.{js,jsx,ts,tsx}'] = 'eslint --max-warnings=0 --no-warn-ignored';
  }

  if (format) {
    config['*'] = 'prettier --write --ignore-unknown';
  }

  if (tsc) {
    config['*.{ts,tsx}'] = 'tsc-files --noEmit';
  }

  return config;
}

export default createLintStagedConfig;
