const custom = require('@digitalroute/cz-conventional-changelog-for-jira/configurable.js');
const types = require('../semantic-release/types.cjs');

/**
 * Conventional Changelog for Jira commitizen adapter.
 *
 * @param options - Commitizen conventional changelog for Jira adapter options.
 *
 * @see https://github.com/digitalroute/cz-conventional-changelog-for-jira
 */
function createCommitizenAdapter(options) {
  return custom({
    types,
    jiraPrefix: 'AWR',
    jiraLocation: 'pre-description',
    jiraPrepend: '[',
    jiraAppend: ']',
    exclamationMark: true,
    jiraOptional: true,
    skipScope: !options?.scopes?.length,
    customScope: !!options?.scopes?.length,
    ...options,
  });
}

module.exports = createCommitizenAdapter;
