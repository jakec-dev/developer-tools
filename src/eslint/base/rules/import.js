/**
 * Import plugin rules.
 *
 * @see https://github.com/import-js/eslint-plugin-import
 * @type { import("@types/eslint").Linter.FlatConfig }
 */
const importRules = {
  rules: {
    'import/order': [
      'error',
      {
        alphabetize: {
          order: 'asc',
          caseInsensitive: true,
        },
        'newlines-between': 'never',
      },
    ],
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: ['**/*.config.js'],
      },
    ],
  },
  settings: {
    // @see https://github.com/import-js/eslint-plugin-import/issues/2556#issuecomment-1419518561
    'import/parsers': {
      espree: ['.js', '.cjs', '.mjs', '.jsx'],
    },
  },
};

export default importRules;
