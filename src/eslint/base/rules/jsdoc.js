/**
 * JSDocs plugin rules.
 *
 * @see https://github.com/gajus/eslint-plugin-jsdoc
 * @type { import("@types/eslint").Linter.FlatConfig }
 */
const jsdocRules = {
  rules: {
    'jsdoc/check-access': 'warn',
    'jsdoc/check-alignment': 'warn',
    'jsdoc/check-examples': 0,
    'jsdoc/check-indentation': 'warn',
    'jsdoc/check-line-alignment': 'warn',
    'jsdoc/check-param-names': 'warn',
    'jsdoc/check-property-names': 'warn',
    'jsdoc/check-syntax': 'warn',
    'jsdoc/check-tag-names': 'warn',
    'jsdoc/check-types': 'warn',
    'jsdoc/check-values': 'warn',
    'jsdoc/empty-tags': 'warn',
    'jsdoc/implements-on-classes': 'warn',
    'jsdoc/informative-docs': 'warn',
    'jsdoc/match-description': 0,
    'jsdoc/multiline-blocks': 'warn',
    'jsdoc/no-bad-blocks': 'warn',
    'jsdoc/no-blank-block-descriptions': 'warn',
    'jsdoc/no-defaults': 'warn',
    'jsdoc/no-missing-syntax': 0,
    'jsdoc/no-multi-asterisks': 'warn',
    'jsdoc/no-restricted-syntax': 0,
    'jsdoc/no-types': 0,
    'jsdoc/no-undefined-types': 0,
    'jsdoc/require-asterisk-prefix': 'warn',
    'jsdoc/require-description': 'warn',
    'jsdoc/require-description-complete-sentence': 'warn',
    'jsdoc/require-example': 0,
    'jsdoc/require-file-overview': 0,
    'jsdoc/require-hyphen-before-param-description': 'warn',
    'jsdoc/require-jsdoc': [
      'warn',
      {
        fixerMessage: ' TODO: Write JSDocs.',
      },
    ],
    'jsdoc/require-param': 'warn',
    'jsdoc/require-param-description': 'warn',
    'jsdoc/require-param-name': 'warn',
    'jsdoc/require-param-type': 0,
    'jsdoc/require-property': 'warn',
    'jsdoc/require-property-description': 'warn',
    'jsdoc/require-property-name': 'warn',
    'jsdoc/require-property-type': 'warn',
    'jsdoc/require-returns': 0,
    'jsdoc/require-returns-check': 'warn',
    'jsdoc/require-returns-description': 'warn',
    'jsdoc/require-returns-type': 0,
    'jsdoc/require-throws': 'warn',
    'jsdoc/require-yields': 'warn',
    'jsdoc/require-yields-check': 'warn',
    'jsdoc/sort-tags': [
      'warn',
      {
        tagSequence: [
          {
            tags: ['@deprecated', 'name', 'summary', 'description'],
          },
          {
            tags: [
              'module',
              'function',
              'constant',
              'class',
              'member',
              'memberOf',
              'extends',
              'param',
              'returns',
            ],
          },
          {
            tags: ['see', 'type', 'example', 'todo'],
          },
        ],
        reportIntraTagGroupSpacing: false,
      },
    ],
    'jsdoc/tag-lines': [
      'warn',
      'always',
      {
        count: 0,
        startLines: 1,
        tags: {
          example: {
            lines: 'any',
          },
        },
      },
    ],
    'jsdoc/valid-types': 'warn',
  },
  settings: {
    jsdoc: {
      tagNamePreference: {
        augments: {
          message: '@extends is to be used over @augments.',
          replacement: 'extends',
        },
      },
    },
  },
};

export default jsdocRules;
