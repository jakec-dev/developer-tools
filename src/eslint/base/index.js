import path from 'path';
import { fileURLToPath } from 'url';
import { FlatCompat } from '@eslint/eslintrc';
import prettierConfig from 'eslint-config-prettier';
import importPlugin from 'eslint-plugin-import';
import jsdocPlugin from 'eslint-plugin-jsdoc';
import importRules from './rules/import.js';
import jsdocRules from './rules/jsdoc.js';

// eslint-disable-next-line no-underscore-dangle
const __filename = fileURLToPath(import.meta.url);
// eslint-disable-next-line no-underscore-dangle
const __dirname = path.dirname(__filename);

const compat = new FlatCompat({
  baseDirectory: __dirname,
});

/**
 * Base eslint configuration.
 *
 * @type { import("@types/eslint").Linter.FlatConfig[] }
 */
const base = [
  {
    ignores: ['**/dist/'],
  },
  {
    plugins: { jsdoc: jsdocPlugin, import: importPlugin },
    languageOptions: {
      parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
      },
    },
  },
  ...compat.extends('airbnb-base'),
  prettierConfig,
  jsdocRules,
  importRules,
];

export default base;
