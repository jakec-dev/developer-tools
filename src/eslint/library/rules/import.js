/**
 * Import plugin rules.
 *
 * @see https://github.com/import-js/eslint-plugin-import
 * @type { import("@types/eslint").Linter.FlatConfig }
 */
const importRules = {
  rules: {
    'import/extensions': 0,
  },
};

export default importRules;
