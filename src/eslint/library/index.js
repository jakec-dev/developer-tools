import base from '../base/index.js';
import importRules from './rules/import.js';

/**
 * ESLint configuration for rollup libraries.
 *
 * @type { import("@types/eslint").Linter.FlatConfig[] }
 */
const library = [...base, importRules];

export default library;
