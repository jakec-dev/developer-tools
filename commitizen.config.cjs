const createCommitizenAdapter = require('./src/commitizen/index.cjs');

const commitizenAdapter = createCommitizenAdapter({
  scopes: [
    'eslint',
    'commitizen',
    'lint-staged',
    'prettier',
    'semantic-release',
    'typescript',
    'multiple',
  ],
});

module.exports = commitizenAdapter;
