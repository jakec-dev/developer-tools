#!/usr/bin/env node
/* eslint-disable no-console */

const { exec } = require('child_process');
const fs = require('fs');
const path = require('path');

/**
 * TODO: Write JSDocs.
 *
 * @param packagePath
 */
async function readPackageJson(packagePath) {
  try {
    const packageJson = await fs.promises.readFile(packagePath, 'utf8');
    return JSON.parse(packageJson);
  } catch (error) {
    console.error('Error reading package.json:', error);
    throw error;
  }
}

/**
 * TODO: Write JSDocs.
 *
 * @param packageJson - The package.json file in a readable format.
 */
function extractPackageManager(packageJson) {
  if (!packageJson || !packageJson.packageManager) {
    return false;
  }
  const [name] = packageJson.packageManager.split('@');
  return name;
}

/**
 * TODO: Write JSDocs.
 *
 * @param packageManager - The name of the package manager.
 * @param developerToolsPackageJson
 */
async function installDevDependencies(packageManager) {
  try {
    const developerToolsPackageJson = await readPackageJson(
      path.join(process.cwd(), 'node_modules', '@jakec-dev', 'developer-tools', 'package.json'),
    );
    const peerDependencies = developerToolsPackageJson.peerDependencies || {};
    const dependencies = Object.keys(peerDependencies).map(
      (dependency) => `${dependency}@${peerDependencies[dependency]}`,
    );

    console.log(`Installing development dependencies using ${packageManager}...`);

    await new Promise((resolve, reject) => {
      const child = exec(
        `${packageManager} add -D ${dependencies.join(' ')}`,
        (error, stdout, stderr) => {
          if (error) {
            console.error(`Error installing dependencies: ${error.message}`);
            reject(error);
          } else if (stderr) {
            console.error(`Error: ${stderr}`);
            reject(new Error(stderr));
          } else {
            console.log(stdout);
            console.log('Development dependencies installed successfully.');
            resolve();
          }
        },
      );

      child.stdout.pipe(process.stdout);
      child.stderr.pipe(process.stderr);
    });
  } catch (error) {
    console.error('Error installing dev dependencies:', error);
    process.exit(1);
  }
}

/**
 * TODO: Write JSDocs.
 */
function createReleaseConfigFile() {
  const content = `import { createReleaseConfig } from '@jakec-dev/developer-tools';
  
  export default createReleaseConfig();

  `;

  fs.writeFileSync(path.join(process.cwd(), './release.config.js'), content);
  console.log('release.config.js file has been created.');
}

/**
 * TODO: Write JSDocs.
 *
 * @param packageJson - The package.json file in a readable format.
 */
async function validatePackageJson(packageJson) {
  try {
    const requiredFields = ['packageManager', 'repository', 'files'];
    const missingFields = requiredFields.filter((field) => !packageJson[field]);

    if (missingFields.length > 0) {
      console.error(
        `The package.json is missing the following required fields: ${missingFields.join(', ')}`,
      );
      process.exit(1);
    } else {
      console.log('package.json contains all required fields.');
    }
  } catch (error) {
    console.error('Error checking package.json:', error);
    process.exit(1);
  }
}

/**
 * TODO: Write JSDocs.
 */
async function run() {
  try {
    const packageJsonPath = path.join(process.cwd(), 'package.json');
    const packageJson = await readPackageJson(packageJsonPath);
    await validatePackageJson(packageJson);
    const packageManager = extractPackageManager(packageJson) || 'pnpm';
    await installDevDependencies(packageManager);
    createReleaseConfigFile();
  } catch (error) {
    console.error('Error running script:', error);
    process.exit(1);
  }
}

run();
