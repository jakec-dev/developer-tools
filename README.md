# Developer Tools

## Installation

### Install

Install `@jakec-dev/developer-tools` and all peerDependencies as devDependencies.

**pnpm**

```sh
pnpm add -D \
  @jakec-dev/developer-tools \
  @semantic-release/changelog@^6.0.3 \
  @semantic-release/git@^10.0.1 \
  @semantic-release/gitlab@^13.0.3 \
  @semantic-release/npm@^12.0.0 \
  commitizen@^4.3.0 \
  conventional-changelog-conventionalcommits@^7.0.2 \
  eslint@^8.57.0 \
  husky@^9.0.11 \
  lint-staged@^15.2.2 \
  prettier@^3.2.5 \
  semantic-release@^23.0.4 \
  semantic-release-jira-notes@^3.0.0 \
  tsc-files@^1.1.4 \
  typescript@^5.4.2
```

**npm**

```sh
npm add -D \
  @jakec-dev/developer-tools \
  @semantic-release/changelog@^6.0.3 \
  @semantic-release/git@^10.0.1 \
  @semantic-release/gitlab@^13.0.3 \
  @semantic-release/npm@^12.0.0 \
  commitizen@^4.3.0 \
  conventional-changelog-conventionalcommits@^7.0.2 \
  eslint@^8.57.0 \
  husky@^9.0.11 \
  lint-staged@^15.2.2 \
  prettier@^3.2.5 \
  semantic-release@^23.0.4 \
  semantic-release-jira-notes@^3.0.0 \
  tsc-files@^1.1.4 \
  typescript@^5.4.2
```

**yarn**

```sh
yarn add -D \
  @jakec-dev/developer-tools \
  @semantic-release/changelog@^6.0.3 \
  @semantic-release/git@^10.0.1 \
  @semantic-release/gitlab@^13.0.3 \
  @semantic-release/npm@^12.0.0 \
  commitizen@^4.3.0 \
  conventional-changelog-conventionalcommits@^7.0.2 \
  eslint@^8.57.0 \
  husky@^9.0.11 \
  lint-staged@^15.2.2 \
  prettier@^3.2.5 \
  semantic-release@^23.0.4 \
  semantic-release-jira-notes@^3.0.0 \
  tsc-files@^1.1.4 \
  typescript@^5.4.2
```

##
